package com.gitlab.demo

data class Velocity private constructor(val value: Double): Comparable<Velocity> {
    companion object {
        fun inMetersPerSecond(value: Double): Velocity {
            return Velocity(value)
        }
    }

    override fun compareTo(other: Velocity): Int {
        return value.compareTo(other.value)
    }
}