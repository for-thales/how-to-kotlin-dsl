package com.gitlab.demo

class LandingPriorityService(val detections: DetectionRepository) {
    fun nextLanding(): Aircraft? {
        if (detections.waitingForLand().isEmpty())
            return null
        val aircraft = detections.waitingForLand().sortedWith(compareByDescending<Aircraft>({it.type.weight}).thenBy({it.position.altitude}))

        detections.hasLanded(aircraft.first())

        return aircraft.first()
    }
}