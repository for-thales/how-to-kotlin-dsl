package com.gitlab.demo

data class Altitude private constructor(val value: Double): Comparable<Altitude> {
    companion object {
        fun fromMeters(value: Double): Altitude {
            return Altitude(value)
        }
    }

    override fun compareTo(other: Altitude): Int {
        return value.compareTo(other.value)
    }
}