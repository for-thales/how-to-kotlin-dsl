package com.gitlab.demo

import kotlin.math.abs

data class Latitude private constructor(val value: Double) {
    companion object {
        fun from(value: Double): Latitude {
            if (abs(value) > 90)
                throw IllegalArgumentException("Latitude must be between -90 and 90")
            return Latitude(value)
        }
    }
}