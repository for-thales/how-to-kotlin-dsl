package com.gitlab.demo

import kotlin.math.abs

data class Longitude private constructor(val value: Double) {
    companion object {
        fun from(value: Double): Longitude {
            if (abs(value) > 180)
                throw IllegalArgumentException("Longitude must be between -90 and 90")
            return Longitude(value)
        }
    }
}