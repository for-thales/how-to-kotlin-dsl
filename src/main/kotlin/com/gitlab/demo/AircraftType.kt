package com.gitlab.demo

enum class AircraftType(val weight: Int) {
    CESNA(757),
    B707(67_500),
    A320(77_000),
    B737(41_140 );
}