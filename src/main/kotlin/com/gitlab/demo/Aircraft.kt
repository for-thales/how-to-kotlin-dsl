package com.gitlab.demo

import java.util.*

data class Aircraft(val id: UUID, val position: Position, val velocity: Velocity, val type: AircraftType) {
    override fun equals(other: Any?): Boolean {
        return other is Aircraft && this.id == other.id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}