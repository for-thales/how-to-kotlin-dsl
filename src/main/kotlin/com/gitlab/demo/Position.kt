package com.gitlab.demo

data class Position(val latitude: Latitude, val longitude: Longitude, val altitude: Altitude)