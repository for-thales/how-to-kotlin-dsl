package com.gitlab.demo

interface DetectionRepository {
    fun waitingForLand(): List<Aircraft>
    fun hasLanded(aircraft: Aircraft)
}