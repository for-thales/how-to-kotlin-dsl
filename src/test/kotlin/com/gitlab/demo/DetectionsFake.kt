package com.gitlab.demo

class DetectionsFake: DetectionRepository {
    val aircrafts = mutableListOf<Aircraft>()

    override fun waitingForLand(): List<Aircraft> {
        return aircrafts
    }
    fun canSee(aircraft: Aircraft) {
        aircrafts.add(aircraft)
    }

    override fun hasLanded(aircraft: Aircraft) {
        aircrafts.remove(aircraft)
    }
}