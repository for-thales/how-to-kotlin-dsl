package com.gitlab.demo

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.*
import kotlin.reflect.KProperty


class ClassifierTest {
    @Test
    fun `should let land a single aircraft`() {
        val cessna = Aircraft(UUID.nameUUIDFromBytes("aircraft-1".toByteArray()),
            Position(Latitude.from(48.0),
                Longitude.from(2.0),
                Altitude.fromMeters(3000.0)
            ),
            Velocity.inMetersPerSecond(40.0),
            AircraftType.CESNA
        )
        val detections = DetectionsFake()
        detections.canSee(cessna)

        val landingPriorityService = LandingPriorityService(detections)

        val aircraftNextToLand = landingPriorityService.nextLanding()

        assertThat(aircraftNextToLand).isEqualTo(cessna)

        given {
            `an aircraft` { id = "aircraft-1" }
        } `when I call` {
            service.nextLanding()
        } `then the result should validate` {
            assertThat(this?.id).isEqualTo("aircraft-1".toId())
        }

    }

    @Test
    fun `should let land in the order`() {
        val cessna1 = Aircraft(UUID.nameUUIDFromBytes("aircraft-1".toByteArray()),
            Position(Latitude.from(48.0),
                Longitude.from(2.0),
                Altitude.fromMeters(3000.0)
            ),
            Velocity.inMetersPerSecond(40.0),
            AircraftType.CESNA
        )
        val cessna2 = Aircraft(UUID.nameUUIDFromBytes("aircraft-2".toByteArray()),
            Position(Latitude.from(48.5),
                Longitude.from(2.3),
                Altitude.fromMeters(3000.0)
            ),
            Velocity.inMetersPerSecond(40.0),
            AircraftType.CESNA
        )
        val detections = DetectionsFake()
        detections.canSee(cessna1)
        detections.canSee(cessna2)

        val landingPriorityService = LandingPriorityService(detections)

        assertThat(landingPriorityService.nextLanding()).isEqualTo(cessna1)

        assertThat(landingPriorityService.nextLanding()).isEqualTo(cessna2)

    }

    @Test
    fun `should give priority to biggest aircraft`() {
        val cessna1 = Aircraft(UUID.nameUUIDFromBytes("aircraft-1".toByteArray()),
            Position(Latitude.from(48.0),
                Longitude.from(2.0),
                Altitude.fromMeters(3000.0)
            ),
            Velocity.inMetersPerSecond(40.0),
            AircraftType.CESNA
        )
        val cessna2 = Aircraft(UUID.nameUUIDFromBytes("aircraft-2".toByteArray()),
            Position(Latitude.from(48.5),
                Longitude.from(2.3),
                Altitude.fromMeters(3000.0)
            ),
            Velocity.inMetersPerSecond(40.0),
            AircraftType.CESNA
        )

        val airbus = Aircraft(UUID.nameUUIDFromBytes("aircraft-3".toByteArray()),
            Position(Latitude.from(47.5),
                Longitude.from(2.2),
                Altitude.fromMeters(3000.0)
            ),
            Velocity.inMetersPerSecond(40.0),
            AircraftType.A320
        )

        val detections = DetectionsFake()
        detections.canSee(cessna1)
        detections.canSee(airbus)
        detections.canSee(cessna2)

        val landingPriorityService = LandingPriorityService(detections)

        assertThat(landingPriorityService.nextLanding()).isEqualTo(airbus)
    }

    @Test
    fun `should give priority to lowest aircraft`() {
        val cessna1 = Aircraft(UUID.nameUUIDFromBytes("aircraft-1".toByteArray()),
            Position(Latitude.from(48.0),
                Longitude.from(2.0),
                Altitude.fromMeters(3000.0)
            ),
            Velocity.inMetersPerSecond(40.0),
            AircraftType.CESNA
        )
        val cessna2 = Aircraft(UUID.nameUUIDFromBytes("aircraft-2".toByteArray()),
            Position(Latitude.from(48.5),
                Longitude.from(2.3),
                Altitude.fromMeters(1500.0)
            ),
            Velocity.inMetersPerSecond(40.0),
            AircraftType.CESNA
        )

        val detections = DetectionsFake()
        detections.canSee(cessna1)
        detections.canSee(cessna2)

        val landingPriorityService = LandingPriorityService(detections)

        assertThat(landingPriorityService.nextLanding()).isEqualTo(cessna2)
    }


    fun given(init: Context.() -> Unit): Context {
        val context = Context()
        context.init()
        return context
    }

    class Context {
        val repository= DetectionsFake()
        val service = LandingPriorityService(repository)

        inner class AircraftBuilder(internal var aircraft: Aircraft) {
            var id: String by Demeter({aircraft.id.toString()}, {aircraft = aircraft.copy(id= it.toId())})
            var altitude: Altitude by Demeter({aircraft.position.altitude}, {aircraft = aircraft.copy(position = aircraft.position.copy(altitude = it))})
        }

        fun `an aircraft`(init: AircraftBuilder.() -> Unit) {
            val builder = AircraftBuilder(dummyAircraft.copy())
            builder.init()
            repository.canSee(builder.aircraft)
        }

        infix fun `when I call`(f: Context.() -> Aircraft?): Aircraft? {
            return this.f()
        }
    }
}

infix fun <T> T.`then the result should validate`(f: T.() -> Unit) {
    f()
}


class Demeter<T>(val get: () -> T, val set: (T) -> Unit) {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): T  = get()
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) = set(value)
}

fun String.toId(): UUID {
    return UUID.nameUUIDFromBytes(this.toByteArray())
}

val dummyAircraft = Aircraft(UUID.randomUUID(),
    Position(Latitude.from(48.0),
        Longitude.from(2.0),
        Altitude.fromMeters(3000.0)
    ),
    Velocity.inMetersPerSecond(40.0),
    AircraftType.CESNA
)

